from flask import Flask, render_template, request, redirect, url_for
from pymongo import MongoClient
import requests
import uuid

userinfo = {'Raman':{'alt':'Image Alternate', 'content': 'Raman Bally, \n Yet to get married \n'},
            'Merryl':{'alt':'Image Alternate', 'content': 'Merryl Dims, \n Married and who knows what \n'},
            'Santosh':{'atl':'Image Alternate', 'content': 'Santy, \nGetting Married, tera kya hoga kaliya\n'}
           }

app = Flask(__name__)
app.debug = True
app.config['GITHUB_CLIENT_ID'] = 'c14d2f7cfcb8bf23d30f'
app.config['GITHUB_CLIENT_SECRET'] = 'd65170583f103c0781fe1b1e91ed68b05c65dd8b'

mongo = MongoClient()
user_collection = mongo.ayyoo.users

@app.route('/')
def index():
    """ index page with a login """
    return render_template('index.html', **{
        'client_id': app.config['GITHUB_CLIENT_ID'],
        'redirect_uri': url_for('github_auth_callback', _external=True),
        'state': str(uuid.uuid4())
    })

@app.route('/about')
def about():
    return render_template('about.html', userinfo=userinfo)

@app.route('/projects')
def projects():
    return render_template('project.html')


@app.route('/_auth_callback')
def github_auth_callback():
    code = request.args.get('code')
    if not code:
        abort(500)
    else:
        data = {
            "client_id": app.config['GITHUB_CLIENT_ID'],
            "client_secret": app.config['GITHUB_CLIENT_SECRET'],
            "code": code
        }
        print(data)
        headers = {
            "Accept": "application/json"
        }
        res = requests.post("https://github.com/login/oauth/access_token",
                            headers = headers,
                            data = data)
        if res.status_code == 200:
            d = res.json()
            if 'error' in d:
                print(d['error'])
                return redirect('/error')
            
            print(d['access_token'])
        print(res.content)
        print("code: {}".format(code))
        #return redirect('/')

if __name__ == '__main__':
    app.run(host='0.0.0.0')


    
